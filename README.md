# Strap

Ensures that a new or existing Mac has necessary development software.

Run it, replacing quoted values with your own:

```bash
curl -sLO https://bitbucket.org/stormpath/mac/raw/master/strap
STRAP_GIT_NAME='Firstname Lastname'
STRAP_GIT_EMAIL='you@domain.com'
STRAP_GITHUB_USER='yourghusername'
bash strap
```

This ensures the following are installed:

* XCode Command Line Tools
* Apple operating system and security updates
* Homebrew
* Homebrew Cask
* bash completion
* openssl
* git
* httpie
* jenv
* liquidprompt
* iTerm2
* Google Chrome
* Oracle JDK 8 with unlimited strength cryptography
* maven
* groovy
* Stormpath CLI
* Docker for Mac
* IntelliJ IDEA
* GMavenPlus Plugin for IntelliJ IDEA
